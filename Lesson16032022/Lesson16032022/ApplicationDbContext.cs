﻿using Lesson16032022.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace Lesson16032022
{
    public class ApplicationDbContext : DbContext
    {
        public DbSet<Team> Teams { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var confiq = new ConfigurationBuilder()
                .SetBasePath(@"D:\Projekts C#\GitRepositoryes\repository16032022\Lesson16032022\Lesson16032022")
                .AddJsonFile("appsettings.json")
                .Build();

            var conectionString = confiq.GetConnectionString("DefaultConnection");
            optionsBuilder.UseSqlServer(conectionString);
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Team>()
                .HasIndex(x => x.Name)
                .IsUnique();

            modelBuilder.Entity<Player>()
                .HasIndex(x => new { x.TeamId, x.JerseyNumber });


            modelBuilder.Entity<Team>()
                .HasOne(x => x.Coach)
                .WithOne(x => x.Team)
                .HasForeignKey<Coach>(x => x.TeamId);
        }
    }
}
