﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lesson16032022.Interface
{
    public interface IEntity<T>
    {
        public T Id { get; set; }
    }
}
