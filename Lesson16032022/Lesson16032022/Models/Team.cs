﻿using Lesson16032022.Enums;
using Lesson16032022.Interface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Lesson16032022.Models
{
    public class Team : IEntity<int>
    {
        /// <summary>
        /// Идентификатор
        /// </summary>
        public int Id { get; set; }

        public int CouchId { get; set; }


        /// <summary>
        /// Название команды
        /// </summary>
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Название города
        /// </summary>
        [Required]
        public string CityName { get; set; }

        /// <summary>
        /// Конфереция
        /// </summary>
        public Conference Conference { get; set; }

        /// <summary>
        /// Год основания
        /// </summary>
        public int FundYear { get; set; }

        /// <summary>
        /// Количество побед
        /// </summary>
        public int WinsCount { get; set; }

        /// <summary>
        /// Количество поражений
        /// </summary>
        public int LossesCount { get; set; }

        public ICollectionId<Team>
    }
}
